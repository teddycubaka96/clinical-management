from rest_framework import serializers
from .models import Clinical

class ClinicalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clinical
        fields = ["designation", "address", "user"]