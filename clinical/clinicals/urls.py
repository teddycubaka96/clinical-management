# from django.conf.urls import url
from django.urls import path, include
from .views import (
    ClinicalApiView,
    ClinicalDetailApiView
)

urlpatterns = [
    path('api', ClinicalApiView.as_view()),
    path('api/<int:clinical_id>/', ClinicalDetailApiView.as_view()),
]