from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from .models import Clinical
from .serializers import ClinicalSerializer

class ClinicalApiView(APIView):
    # add permission to check if user is authenticated
    permission_classes = [permissions.IsAuthenticated]

    # 1. List all
    def get(self, request, *args, **kwargs):
        '''
        List all the todo items for given requested user
        '''
        clinical = Clinical.objects.filter(user = request.user.id)
        serializer = ClinicalSerializer(clinical, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
        # return Response({"message" : "chée"}, status=status.HTTP_200_OK)

    # 2. Create
    def post(self, request, *args, **kwargs):
        '''
        Create the Todo with given todo data
        '''
        data = {
            "designation": request.data.get("designation"), 
            "address": request.data.get("address"),
            "user": request.user.id
        }
        serializer = ClinicalSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ClinicalDetailApiView(APIView):
    # add permission to check if user is authenticated
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, clinical_id):
        '''
        Helper method to get the object with given todo_id
        '''
        try:
            return Clinical.objects.get(id=clinical_id)
        except Clinical.DoesNotExist:
            return None

    # 3. Retrieve
    def get(self, request, clinical_id, *args, **kwargs):
        '''
        Retrieves the Clinic with given todo_id
        '''
        clinical_instance = self.get_object(clinical_id)
        if not clinical_instance:
            return Response(
                {"res": "Object with todo id does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = ClinicalSerializer(clinical_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # 4. Update
    def put(self, request, clinical_id, *args, **kwargs):
        '''
        Updates the todo item with given todo_id if exists
        '''
        todo_instance = self.get_object(clinical_id)
        if not todo_instance:
            return Response(
                {"res": "Object with clinical id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        data = {
             "designation": request.data.get("designation"), 
            "address": request.data.get("address"),
        }
        serializer = ClinicalSerializer(instance = todo_instance, data=data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # 5. Delete
    def delete(self, request, clinical_id, *args, **kwargs):
        '''
        Deletes the clinical item with given clinical_id if exists
        '''
        clinical_instance = self.get_object(clinical_id)
        if not clinical_instance:
            return Response(
                {"res": "Object with clinical id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        clinical_instance.delete()
        return Response(
            {"res": "Object deleted!"},
            status=status.HTTP_200_OK
        )