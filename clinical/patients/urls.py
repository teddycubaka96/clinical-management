# from django.conf.urls import url
from django.urls import path, include
from .views import (
    PatientApiView,
    PatientDetailApiView
)

urlpatterns = [
    path('api', PatientApiView.as_view()),
    path('api/<int:patient_id>/', PatientDetailApiView.as_view()),
]