from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from .models import Patient
from .serializers import PatientSerializer

class PatientApiView(APIView):
    # add permission to check if user is authenticated
    permission_classes = [permissions.IsAuthenticated]

    # 1. List all
    def get(self, request, *args, **kwargs):
        '''
        List all the todo items for given requested user
        '''
        patient = Patient.objects.filter(user = request.user.id)
        serializer = PatientSerializer(patient, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
        # return Response({"message" : "chée"}, status=status.HTTP_200_OK)

    # 2. Create
    def post(self, request, *args, **kwargs):
        '''
        Create the Todo with given todo data
        '''
        data = {
            "firstName": request.data.get("firstName"), 
            "LastName": request.data.get("LastName"), 
            "bloodGroup": request.data.get("bloodGroup"), 
            "sexe": request.data.get("sexe"), 
            "phone": request.data.get("phone"), 
            "job": request.data.get("job"),
            "user": request.user.id
        }
        serializer = PatientSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PatientDetailApiView(APIView):
    # add permission to check if user is authenticated
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, patient_id):
        '''
        Helper method to get the object with given todo_id
        '''
        try:
            return Patient.objects.get(id=patient_id)
        except Patient.DoesNotExist:
            return None

    # 3. Retrieve
    def get(self, request, patient_id, *args, **kwargs):
        '''
        Retrieves the Clinic with given todo_id
        '''
        patient_instance = self.get_object(patient_id)
        if not patient_instance:
            return Response(
                {"res": "Object with todo id does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = PatientSerializer(patient_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # 4. Update
    def put(self, request, patient_id, *args, **kwargs):
        '''
        Updates the todo item with given todo_id if exists
        '''
        todo_instance = self.get_object(patient_id)
        if not todo_instance:
            return Response(
                {"res": "Object with patient id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        data = {
             "designation": request.data.get("designation"), 
            "address": request.data.get("address"),
        }
        serializer = PatientSerializer(instance = todo_instance, data=data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # 5. Delete
    def delete(self, request, patient_id, *args, **kwargs):
        '''
        Deletes the patient item with given patient_id if exists
        '''
        patient_instance = self.get_object(patient_id)
        if not patient_instance:
            return Response(
                {"res": "Object with patient id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        patient_instance.delete()
        return Response(
            {"res": "Object deleted!"},
            status=status.HTTP_200_OK
        )