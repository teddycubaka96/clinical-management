from django.db import models
from django.contrib.auth.models import User

class Patient(models.Model):
    firstName = models.CharField(max_length = 15)
    LastName = models.CharField(max_length=15)
    bloodGroup = models.CharField(max_length=15)
    sexe = models.CharField(max_length=15)
    phone = models.IntegerField()
    job = models.CharField(max_length=20)
    user = models.ForeignKey(User, on_delete = models.CASCADE, blank = True, null = True)

    def __str__(self):
        return self.firstName
