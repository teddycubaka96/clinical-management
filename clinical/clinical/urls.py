from django.contrib import admin
from django.urls import path, include
from clinicals import urls as clinical_urls
from patients import urls as patient_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('clinical/', include(clinical_urls)),
    path('patient/', include(patient_urls)),
]